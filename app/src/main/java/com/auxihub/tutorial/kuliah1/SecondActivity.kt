package com.auxihub.tutorial.kuliah1

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_USERNAME = "EXTRA_USERNAME"

        fun getStartIntent(context: Context, username: String) = Intent(context, SecondActivity::class.java).apply {
            putExtra(EXTRA_USERNAME, username)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        username.text = "Welcome, ${intent.getStringExtra(EXTRA_USERNAME)}"
    }
}
