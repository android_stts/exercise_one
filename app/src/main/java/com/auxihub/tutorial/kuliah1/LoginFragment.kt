package com.auxihub.tutorial.kuliah1

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.login_fragment.*

/**
 * @author erick
 * @since 2018-10-09
 */
class LoginFragment : Fragment() {

    private lateinit var listener: Listener

    companion object {
        fun newInstance(listener: Listener) = LoginFragment().apply {
            this.listener = listener
        }
    }

    interface Listener {
        fun onSuccess(username: String)
        fun onError(message: String)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        login_button.setOnClickListener {
            val username = username.text.toString()
            val password = password.text.toString()
            if (username.isBlank() || password.isBlank()) {
                listener.onError("Username dan Password tidak boleh kosong")
            } else if (password == "stts") {
                listener.onSuccess(username)
            } else {
                listener.onError("Passwordnya harus `stts`")
            }
        }
    }
}