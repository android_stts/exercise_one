package com.auxihub.tutorial.kuliah1

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.login_status_fragment.*

/**
 * @author erick
 * @since 2018-10-09
 */
class LoginStatusFragment : Fragment() {
    companion object {
        fun newInstance() = LoginStatusFragment()
    }

    private var messages = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_status_fragment, container, false)
    }

    fun addMessage(message: String) {
        messages += "$message\n"
        status_container.text = messages
    }
}