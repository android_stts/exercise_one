package com.auxihub.tutorial.kuliah1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity(), LoginFragment.Listener {

    private lateinit var loginFragment: LoginFragment
    private lateinit var loginStatusFragment: LoginStatusFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loginFragment = LoginFragment.newInstance(this)
        loginStatusFragment = LoginStatusFragment.newInstance()

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.first_fragment_container, loginFragment)
            replace(R.id.second_fragment_container, loginStatusFragment)
            commit()
        }
    }

    override fun onSuccess(username: String) {
        startActivity(SecondActivity.getStartIntent(this, username))
    }

    override fun onError(message: String) {
        loginStatusFragment.addMessage(message)
    }
}
